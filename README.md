# Usage

To install the dependencies

```shell
$ npm install
```

To compile code (This uses ES6 syntax)

```shell
$ npm run compile
```

To Run code

```shell
$ npm start
```

To Run tests

```shell
$ npm test
```

