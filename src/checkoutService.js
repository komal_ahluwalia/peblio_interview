import { map, reduce, filter, first } from 'underscore';

export function calculateCartTotal(itemPricingMasterList, specialPricingMasterList, checkedOutItemList) {
    const priceArray = getPriceTotalPerItem(itemPricingMasterList, specialPricingMasterList, checkedOutItemList);
    return reduce(priceArray, (accumulator, value) => accumulator + value);
}

function getPriceTotalPerItem(itemPricingMasterList, specialPricingMasterList, checkedOutItemList) {
    return map(checkedOutItemList,
        checkedOutItem => getPriceTotal(checkedOutItem, specialPricingMasterList, itemPricingMasterList));
}

function getPriceTotal(checkedOutitem, specialPricingMasterList, itemPricingMasterList) {
    const checkedOutQuantity = checkedOutitem.getQuantity();
    const checkedOutSku = checkedOutitem.getSku();
    const itemPrice = getPriceForSku(itemPricingMasterList, checkedOutSku);
    if (isItemSpecialPriced(checkedOutQuantity, checkedOutSku, specialPricingMasterList)) {
        const itemSpecialPrice = getPriceForSku(specialPricingMasterList, checkedOutSku);
        return getPriceForSpecialPricedItem(checkedOutQuantity, itemPrice, itemSpecialPrice);
    }
    return getTotalPriceForItem(checkedOutQuantity, itemPrice);
}

function getPriceForSpecialPricedItem(checkedOutQuantity, itemPrice, itemSpecialPrice) {
    const unitsAtOriginalPrice = getUnitsAtOriginalPrice(checkedOutQuantity, itemSpecialPrice);
    const unitsAtSpecialPrice = getUnitsAtSpecialPrice(checkedOutQuantity, unitsAtOriginalPrice, itemSpecialPrice);
    return (unitsAtOriginalPrice * itemPrice.getPrice()) + (itemSpecialPrice.getPrice() * unitsAtSpecialPrice);
}

function isItemSpecialPriced(checkedOutQuantity, checkedOutSku, specialPricingMasterList) {
    const itemSpecialPrice = getPriceForSku(specialPricingMasterList, checkedOutSku);
    return itemSpecialPrice !== undefined && checkedOutQuantity >= itemSpecialPrice.getQuantity();
}

function getTotalPriceForItem(checkedOutQuantity, itemPrice) {
    return checkedOutQuantity * itemPrice.getPrice();
}

function getUnitsAtOriginalPrice(checkedOutQuantity, itemSpecialPrice) {
    return (checkedOutQuantity % itemSpecialPrice.getQuantity());
}

function getUnitsAtSpecialPrice(checkedOutQuantity, unitsAtOriginalPrice, itemSpecialPrice) {
    return (checkedOutQuantity - unitsAtOriginalPrice) / itemSpecialPrice.getQuantity()
}

function getPriceForSku(itemPricingMasterList, itemSku) {
    return first(filter(itemPricingMasterList, itemPricing => itemPricing.getSku() === itemSku));
}