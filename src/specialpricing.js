import ItemPricing from "./itempricing";

export default class SpecialPricing extends ItemPricing {
    constructor(sku, price, quantity) {
        super(sku, price);
        this.quantity = quantity;
    }
    getQuantity() {
        return this.quantity;
    }
}