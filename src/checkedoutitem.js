export default class CheckedOutItem {
    constructor(sku, quantity) {
        this.sku = sku;
        this.quantity = quantity;
    }
    getSku() {
        return this.sku;
    }
    getQuantity() {
        return this.quantity;
    }
}