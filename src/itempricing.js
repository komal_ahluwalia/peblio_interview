export default class ItemPricing {
    constructor(sku, price) {
        this.sku = sku;
        this.price = price;
    }
    getSku() {
        return this.sku;
    }
    getPrice() {
        return this.price;
    }
}