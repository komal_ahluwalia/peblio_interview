import ItemPricing from './itempricing';
import SpecialPricing from './specialpricing';
import CheckedOutItem from './checkedoutitem';

export const itemA = new ItemPricing("A", 13.0);
export const itemSpecialPricingA = new SpecialPricing("A", 30.0, 3);
export const itemB = new ItemPricing("B", 16.0);
export const itemSpecialPricingB = new SpecialPricing("B", 29.0, 2);
export const itemC = new ItemPricing("C", 18.0);


export const checkedOutItemA = new CheckedOutItem("A", 8);
export const checkedOutItemB = new CheckedOutItem("B", 5);
export const checkedOutItemC = new CheckedOutItem("C", 3);

export const checkOutItems = [checkedOutItemA, checkedOutItemB, checkedOutItemC];
export const itemPricingMasterList = [itemA, itemB, itemC];
export const itemSpecialPricingMasterList = [itemSpecialPricingA, itemSpecialPricingB];
