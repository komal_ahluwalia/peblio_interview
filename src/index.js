import { calculateCartTotal } from './checkoutService';
import { checkOutItems, itemPricingMasterList, itemSpecialPricingMasterList } from './testData';


export function getCartTotal() {
    return calculateCartTotal(itemPricingMasterList, itemSpecialPricingMasterList, checkOutItems);
}

console.log("The Cart Total is: " + getCartTotal());


