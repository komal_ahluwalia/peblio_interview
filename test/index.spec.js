import { getCartTotal } from '../src/index';
import { stub, assert } from 'sinon';
import * as CheckoutService from '../src/checkoutService';
import { expect } from 'chai';
import { describe, it } from 'mocha';
const calculateCartTotalStub = stub(CheckoutService, "calculateCartTotal").returns(124);
import { checkOutItems, itemPricingMasterList, itemSpecialPricingMasterList } from '../src/testData';

describe('index', function () {

    describe('getCartTotal', function () {
        it('should return cart total given by calculateCartTotal', function () {
            expect(getCartTotal()).to.be.deep.equal(124);
            assert.calledWith(calculateCartTotalStub, itemPricingMasterList, itemSpecialPricingMasterList, checkOutItems);
        });
    });

});