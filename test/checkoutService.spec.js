import {
    itemPricingMasterList,
    itemSpecialPricingMasterList,
    checkOutItems,
    itemSpecialPricingA,
    itemA
} from '../src/testData';
import { expect } from 'chai';
import { describe, it } from 'mocha';
import ItemPricing from '../src/itempricing';
import rewire from 'rewire';
import CheckedOutItem from '../src/checkedoutitem';
const checkoutService = rewire('../src/checkoutService');
const getPriceForSku = checkoutService.__get__('getPriceForSku');
const getUnitsAtOriginalPrice = checkoutService.__get__('getUnitsAtOriginalPrice');
const getUnitsAtSpecialPrice = checkoutService.__get__('getUnitsAtSpecialPrice');
const isItemSpecialPriced = checkoutService.__get__('isItemSpecialPriced');
const calculateCartTotal = checkoutService.__get__('calculateCartTotal');
const getPriceTotalPerItem = checkoutService.__get__('getPriceTotalPerItem');
const getPriceTotal = checkoutService.__get__('getPriceTotal');
const getPriceForSpecialPricedItem = checkoutService.__get__('getPriceForSpecialPricedItem');

describe('CheckoutService', function () {

    describe('getPriceForSku', function () {
        it('should return price if valid sku passed', function () {
            const priceForSKU = getPriceForSku(itemPricingMasterList, "A");
            expect(priceForSKU).to.be.deep.equal(new ItemPricing("A", 13));
        });

        it('should not return price if invalid sku passed', function () {
            const priceForSKU = getPriceForSku(itemPricingMasterList, "D");
            expect(priceForSKU).to.be.undefined;
        });
    });

    describe('getSpecialPriceForSku', function () {
        it('should return special price if valid sku passed', function () {
            const priceForSKU = getPriceForSku(itemSpecialPricingMasterList, "A");
            expect(priceForSKU).to.be.deep.equal(itemSpecialPricingA);
        });

        it('should not return special price if invalid sku passed', function () {
            const priceForSKU = getPriceForSku(itemSpecialPricingMasterList, "C");
            expect(priceForSKU).to.be.undefined;
        });
    });

    describe('getUnitsAtOriginalOrice', function () {
        it('should return remainder units at original price when quantity is not multiple of special pricing units', function () {
            const unitsAtOriginalPrice = getUnitsAtOriginalPrice(10, itemSpecialPricingA);
            expect(unitsAtOriginalPrice).to.be.equal(1);
        });

        it('should return 0 units at original price when quantity is multiple of special pricing units', function () {
            const unitsAtOriginalPrice = getUnitsAtOriginalPrice(9, itemSpecialPricingA);
            expect(unitsAtOriginalPrice).to.be.equal(0);
        });
    });

    describe('getUnitsAtSpecialPrice', function () {
        it('should return remainder units at original price when quantity is not multiple of special pricing units', function () {
            const unitsAtSpecialPrice = getUnitsAtSpecialPrice(10, 1, itemSpecialPricingA);
            expect(unitsAtSpecialPrice).to.be.equal(3);
        });

        it('should return 0 units at original price when quantity is multiple of special pricing units', function () {
            const unitsAtSpecialPrice = getUnitsAtSpecialPrice(9, 9, itemSpecialPricingA);
            expect(unitsAtSpecialPrice).to.be.equal(0);
        });
    });

    describe('isItemSpecialPriced', function () {
        it('should return true if item sku has a special pricing available', function () {
            const isSpecialPriced = isItemSpecialPriced(10, "A", itemSpecialPricingMasterList);
            expect(isSpecialPriced).to.be.equal(true);
        });

        it('should return false if item sku has a special pricing available', function () {
            const isSpecialPriced = isItemSpecialPriced(10, "C", itemSpecialPricingMasterList);
            expect(isSpecialPriced).to.be.equal(false);
        });

        it('should return false if item sku quantity is less than special pricing quantity', function () {
            const isSpecialPriced = isItemSpecialPriced(2, "A", itemSpecialPricingMasterList);
            expect(isSpecialPriced).to.be.equal(false);
        });
    });

    describe('getPriceForSpecialPricedItem', function () {
        it('should return special priced for item when checkedOutQuantity not a multiple of specialpriced Quantity', function () {
            const specialPrice = getPriceForSpecialPricedItem(10, itemA, itemSpecialPricingA);
            expect(specialPrice).to.be.equal((30 * 3) + 13);
        });

        it('should return special priced for item when checkedOutQuantity a multiple of specialpriced Quantity', function () {
            const specialPrice = getPriceForSpecialPricedItem(9, itemA, itemSpecialPricingA);
            expect(specialPrice).to.be.equal((30 * 3));
        });
    });

    describe('getPriceTotal', function () {
        it('should return getPriceTotal for boughtQuantity as a sum of regular and special pricing', function () {
            const totalPrice = getPriceTotal(new CheckedOutItem("A", 10), itemSpecialPricingMasterList, itemPricingMasterList);
            expect(totalPrice).to.be.equal((30 * 3) + 13);
        });

        it('should return getPriceTotal for boughtQuantity as a sum of regular pricing when special pricing does not exist', function () {
            const totalPrice = getPriceTotal(new CheckedOutItem("C", 10), itemSpecialPricingMasterList, itemPricingMasterList);
            expect(totalPrice).to.be.equal(180);
        });

        it('should return getPriceTotal for boughtQuantity as a sum of regular pricing when special pricing does not apply', function () {
            const totalPrice = getPriceTotal(new CheckedOutItem("B", 1), itemSpecialPricingMasterList, itemPricingMasterList);
            expect(totalPrice).to.be.equal(16);
        });
    });

    describe('getPriceTotalPerItem', function () {
        it('should return getPriceTotalPerItem for boughtQuantity array as a sum of regular and special pricing', function () {
            const totalPrice = getPriceTotalPerItem(itemPricingMasterList, itemSpecialPricingMasterList, [new CheckedOutItem("A", 10)]);
            expect(totalPrice).to.be.deep.equal([(30 * 3) + 13]);
        });

        it('should return getPriceTotal for boughtQuantity array as a sum of regular pricing when special pricing does not exist', function () {
            const totalPrice = getPriceTotalPerItem(itemPricingMasterList, itemSpecialPricingMasterList, [new CheckedOutItem("C", 10), new CheckedOutItem("A", 10)]);
            expect(totalPrice).to.be.deep.equal([180, (30 * 3) + 13]);
        });

        it('should return getPriceTotal for boughtQuantity as a sum of regular pricing when special pricing does not apply', function () {
            const totalPrice = getPriceTotalPerItem(itemPricingMasterList, itemSpecialPricingMasterList, [new CheckedOutItem("B", 1), new CheckedOutItem("C", 10), new CheckedOutItem("A", 10)]);
            expect(totalPrice).to.be.deep.equal([16, 180, 103]);
        });
    });

    describe('calculateCartTotal', function () {
        it('should calculateCartTotal', function () {
            const cartTotal = calculateCartTotal(itemPricingMasterList, itemSpecialPricingMasterList, checkOutItems)
            expect(cartTotal).to.be.equal(214);
        })
    });

});